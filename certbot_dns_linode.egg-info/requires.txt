dns-lexicon>=2.2.3
setuptools
zope.interface
acme>=0.31.0
certbot>=1.1.0

[:python_version < "3.3"]
mock

[docs]
Sphinx>=1.0
sphinx_rtd_theme
